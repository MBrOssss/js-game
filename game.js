const appDiv = document.getElementById('app');

var config = {
    type: Phaser.AUTO,
    width: 900,
    height: 600,
    parent: appDiv,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};


var game = new Phaser.Game(config);

function preload() {

    this.load.image('background', 'pano.png');
    this.load.image('alien', 'creature.png');
    this.load.image('bullet', 'rakieta.png');
    this.load.image('comet', 'kometa.png')
    this.load.spritesheet('player', 'p.png', { frameWidth: 64, frameHeight: 48 });
    this.load.spritesheet('boom', 'animacja.png', { frameWidth: 64, frameHeight: 48});

}

var player;
var cursors;
var fireButton;
var comets;
var bullet;
var bullets;
var boom;
var bulletTime = 0;
var score = 0;
var life = 2;
var killed = 0;
var gameOver = false;
var gameWin = false;

function create(){
    let back = this.add.tileSprite(0, 0, 2700, 600, 'background');
    back.setOrigin(0)
    this.cameras.main.setBounds(0, 0, 2700, 600);
    this.physics.world.setBounds(0, 0, 2700, 600)

    player = this.physics.add.sprite(10, 280, 'player');
    player.setCollideWorldBounds(true);
    this.cameras.main.startFollow(player)

    scoreText = this.add.text(20, 20, 'Score: ' + score);
    lifeText = this.add.text(400, 20, 'Life: ' + life);
    scoreText.setScrollFactor(0,0);
    lifeText.setScrollFactor(0,0);

    this.anims.create({
        key: 'Boom',
        frames: this.anims.generateFrameNumbers('boom', { start: 0, end: 2 }),
        frameRate: 10,
        repeat: 0
    });

    this.anims.create({
        key: 'Up',
        frames: this.anims. generateFrameNumbers('player', {start: 0, end: 0}),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'Down',
        frames: this.anims. generateFrameNumbers('player', {start: 0, end: 4}),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'Front',
        frames: [{ key: 'player', frame: 0 }],
        frameRate: 20
    });

    cursors = this.input.keyboard.createCursorKeys();
    fireButton = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

    //ALIENS
    let _x = 700;
    let _y = 0;
    aliens = this.physics.add.staticGroup();
    for (var x = 0; x < 2; x++)
    {
        for (var y = 0; y < 8; y++)
        {
            var alien = aliens.create(_x + 60, _y + 60, 'alien')
            _y = _y + 60;
        }
        _y = 0;
        _x = _x + 60;
    }
    _x = 1500
    for (var x = 0; x < 3; x++)
    {
        for (var y = 0; y < 8; y++)
        {
            var alien = aliens.create(_x + 60, _y + 60, 'alien')
            _y = _y + 60;
        }
        _y = 0;
        _x = _x + 60;
    }
    _x = 2400
    for (var x = 0; x < 4; x++)
    {
        for (var y = 0; y < 8; y++)
        {
            var alien = aliens.create(_x + 60, _y + 60, 'alien')
            _y = _y + 60;
        }
        _y = 0;
        _x = _x + 60;
    }

    aliens.getChildren().forEach(c => c.setScale(0.5).setOrigin(0).refreshBody());
    this.physics.add.collider(player, aliens);

    comets = this.physics.add.group();
    bullets =this.physics.add.group();
    bullets.createMultiple(30, 'bullet');
    this.physics.add.overlap(bullets, aliens, collisionHandler, null, this);
    this.physics.add.overlap(player, comets, collisionHandlerPlayer, null, this);

    function collisionHandler (bullet, alien)
    {
    
        killed ++;
        alien.disableBody(true, true);
        bullet.disableBody(true,true);
        var ogien = this.physics.add.sprite(alien.x, alien.y, 'boom');
        ogien.anims.play('Boom', true);
        ogien.once('animationcomplete', () => {
            ogien.destroy()
        })
        score = score + 10;
        scoreText.setText('Score: ' + score);

        var comet = comets.create(alien.x,alien.y, 'comet');
        comet.setBounce(1);
        comet.setCollideWorldBounds(false);
        if(killed < 16){
            comet.setVelocityX(-500);
        }
        if(killed >= 16 && killed < 40){
            comet.setVelocity(-600,100);
        }
        if(killed >= 40){
            if(killed % 2 == 1){
                comet.setVelocity(-700,100);
            }
            else{
                comet.setVelocity(-700,-100);
            }

        }

        if(killed == 16){
            life++;
            lifeText.setText('Life: ' + life);
        }
        
        if(killed == 40){
            life++;
            lifeText.setText('Life: ' + life);  
        }
       if(killed == 72){
            gameWin = true;
            this.physics.pause();
        }
            
    
    }

    function collisionHandlerPlayer (player, comet)
    {
        life--;
        lifeText.setText('Life: ' + life);
        comet.disableBody(true, true);
        var ogien = this.physics.add.sprite(player.x, player.y, 'boom');
        ogien.anims.play('Boom', true);
        ogien.once('animationcomplete', () => {
            ogien.destroy()
        })
        if(life <= 0){
        gameOverText = this.add.text(player.x, 300, 'GAME OVER', { fontSize: '48px', fill: '#FFFFFF'});
        this.physics.pause();
        gameOver = true;
        }
    }

    player.body.immovable = false;
}
var nextBulletTime = 0;

function update(){

    if(cursors.left.isDown){
        player.setVelocityX(-150);
    }
    else if(cursors.right.isDown){
        player.setVelocityX(150);
    }
    else{
        player.setVelocityX(0);
        player.setVelocityY(0);
        player.anims.play('Front', true);
    }
    if(cursors.up.isDown){
        player.setVelocityY(-150);
        player.anims.play('Up', true);
    }
    else if(cursors.down.isDown){
        player.setVelocityY(150);
        player.anims.play('Down', true);
    }



    if(gameOver){
        gameOverText = this.add.text(player.x, 300, 'GAME OVER', { fontSize: '48px', fill: '#FFFFFF' });
        gameOverText = this.add.text(player.x, 400, 'Your score: ' + score, { fontSize: '48px', fill: '#FFFFFF' });
    }

    if(gameWin){
        gameWinText = this.add.text(player.x, 300, 'YOU WON', { fontSize: '48px', fill: '#FFFFFF' });
        gameWinText = this.add.text(player.x, 400, 'Your score: ' + score, { fontSize: '48px', fill: '#FFFFFF' });
    }

   if(fireButton.isDown && game.getTime() >= nextBulletTime){
    var bullet = bullets.create(player.x, player.y+10, 'bullet');
    bullet.setCollideWorldBounds(false);
    bullet.setVelocity(400, 0);
    nextBulletTime = game.getTime() + 200;

   }
}
